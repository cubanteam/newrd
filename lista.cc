#include "lista.h"

using namespace std;

int main() {

  
    vector<data> sdata1;
    vector<data> sdata2;
    sdata1.reserve(10000);
    sdata2.reserve(10000);
    
    read_data(sdata1,sdata2);
    
    /*
    for(int i =0; i<sdata1.size(); i++)
    {
        sdata1[i].print();
    }
	
	
	
	    for(int i =0; i<sdata2.size(); i++)
    {
        sdata2[i].print();
    }
    
    */
    
       // daily_selector(sdata1);
       // daily_selector(sdata2);
    
    
        mon_selector(sdata1,false);
        mon_selector(sdata2,false);
        system("gnome-terminal -x sh -c 'mv *.pdf mensual;  exit ; exec bash'");
        mon_selector(sdata1,true);
        mon_selector(sdata2,true);
        system("gnome-terminal -x sh -c 'mv *.pdf time1118;  exit ; exec bash'");
        
        
        cout << endl << "Moving to hour plots" << endl;
        
        
        vector<vector<ddata>> mon_hour;
        vector<vector<ddata>> mon_hour_err;
        vector<ddata> v1, v2;
        
        for(int i= 1; i < 25; i++){
        mon_hour_selector(sdata1,i,v1,v2);
        mon_hour.push_back(v1);
        mon_hour_err.push_back(v2);
        v1.erase(v1.begin(),v1.begin()+v1.size());
        v2.erase(v2.begin(),v2.begin()+v2.size());
        }
       
      /* 
       for(int i = 0 ; i < 25; i ++) 
       cout << test.size() << " " << test[0].size() << endl ; 
     */   
   
      mon_hour_plotter(mon_hour,mon_hour_err); 
      system("gnome-terminal -x sh -c 'mv *.pdf hora;  exit ; exec bash'");  
    
      
	return 0;
}


void read_data(std::vector<data>& v,std::vector<data>& v1)
{
    string fname = "/home/jose/Projects/newrd/CETESB_merge_2010_2015_limpo.csv";
    
 //    cout << fname <<endl;
   
   
    fstream file;
    
    file.open(fname, ios::in);

    
    data d;

  
    
    int counter = 0;
   	while (true) {      
		if (!file.good ()) break;  
        string pdline, Stemp, token;
		double Dtemp;
		int Itemp;
        
      
        getline(file, pdline);
//        cout << pdline << endl;
        
        istringstream ss(pdline);
        
            getline(ss , token, ',');
            d.code= token;
            getline(ss , token, ',');
            d.Estacao_id= token;
            getline(ss , token, ',');
            d.Estacao_id2= token;
            getline(ss , token, ',');
            d.Hora_id= token;
            getline(ss , token, ',');
            d.Data_id = token;
            getline(ss , token, ',');
            d.pm10medio= token;
            getline(ss , token, ',');
            d.pm25medio= token;
            getline(ss , token, ',');
            d.NOmedio= token;
            getline(ss , token, ',');
            d.NO2medio= token;
            getline(ss , token, ',');
            d.NOxmedio= token;
            getline(ss , token, ',');
            d.SO2medio= token;
            getline(ss , token, ',');
            d.O3medio= token;
            
            if(d.Estacao_id=="5"){
          //  d.print();
            v.push_back(d);    
            counter++;
            }
            else if(d.Estacao_id=="31"){
            v1.push_back(d);    
            counter++;
            }
       /*     
        while(getline(ss , token, ',')) {
                cout << token << '\n';
                }
        */
        
    /*    counter++;
        cout << counter <<endl;*/
    }
    
   // cout << counter <<endl;
}


void daily_selector(std::vector<data>& v){
   
    
   int year, mon, day, time;
   int dday;
   
   vector<ddata > vdia,vddia; 
   
   ddata dia, counter;
   dia.setceros(); 
   counter.setceros();
   

   info_select(v[0],year,mon,day, time); 
   dday = day;
   dia.readbasics(v[0]);
   
   for(int i = 0; i < v.size() ; i++){
       
    info_select(v[i],year,mon,day, time); 

    if(dday==day){
        dia.esum(v[i],counter);
    //    cout << "siii" <<endl;
    }
   else {
       dia.edivision(counter);
       vdia.push_back(dia);
    //   cout << "nooo " << endl;
       dday = day;
       counter.setceros();
       dia.setceros();
       dia.readbasics(v[i]);
       dia.esum(v[i],counter);
    }
    
    

    
      cout << "printing daily data " << endl;
/*
        for(int i =0; i<vdia.size(); i++)
    {
        vdia[i].print();
    } */
    
    cout << " other data set " << endl  << endl;
    
    
      info_select(v[0],year,mon,day, time); 
     dday = day;
     dia.readbasics(v[0]);
     counter.setceros();

    dia.setceros();
  //  mes.print();
 //   cout << endl;
    for(int i = 0; i < v.size() ; i++){ // v.size()
       
    info_select(v[i],year,mon,day, time); 

    if(dday==day){
     //   v[i].print();
     //   vmon[mon-1].print();
        dia.e2sum(v[i],vdia[day-1],counter);
     //   mes.print();
     //   cout << endl;
    //    cout << "siii" <<endl;
    }
   else {
       dia.edivision(counter);
       dia.esqrt();
       vddia.push_back(dia);
    //   cout << "nooo " << endl;
       dday = day;
       counter.setceros();
       dia.setceros();
       dia.readbasics(v[i]);
       dia.setceros();
        dia.e2sum(v[i],vdia[day-1],counter);
    }
   
}

/*
for(int i =0; i<vdia.size(); i++)
    {
        vdia[i].print();
        vddia[i].print();
        cout << endl; 
    }

*/
   
}

}

void mon_selector(std::vector<data>& v, bool small){
   
    
   int year, mon, day, time;
   int dmon;
      
   vector<ddata > vmon; 
   vector<ddata > vdmon; 
   
   ddata mes;
   ddata counter;
   mes.setceros(); 
   counter.setceros();
   

   info_select(v[0],year,mon,day, time); 
   dmon = mon;
   mes.readbasics(v[0]);
   
   for(int i = 0; i < v.size() ; i++){
       
    info_select(v[i],year,mon,day, time); 

    if(year<2012)
        continue;
    
    if((time < 11 || time > 18) && small)
        continue;
    
    if(dmon==mon){
        mes.esum(v[i],counter);
    }
   else {
       mes.edivision(counter);
       vmon.push_back(mes);
    //   cout << "nooo " << endl;
       dmon = mon;
       counter.setceros();
       mes.setceros();
       mes.readbasics(v[i]);
       mes.esum(v[i],counter);
    }
    
    
    
    }
    

    
     info_select(v[0],year,mon,day, time); 
     dmon = mon;
     mes.readbasics(v[0]);
     counter.setceros();

    mes.setceros();
    cout << endl;
    for(int i = 0; i < v.size() ; i++){ // v.size()
       
    info_select(v[i],year,mon,day, time); 
    
    if(year<2012)
        continue;

    if(dmon==mon){
        mes.e2sum(v[i],vmon[mon-1],counter);
    }
   else {
       mes.edivision(counter);
       mes.esqrt();
       vdmon.push_back(mes);
    //   cout << "nooo " << endl;
       dmon = mon;
       counter.setceros();
       mes.setceros();
       mes.readbasics(v[i]);
       mes.setceros();
       mes.e2sum(v[i],vmon[mon-1],counter);
    }
   
}

/*for(int i =0; i<vmon.size(); i++)
    {
        vmon[i].print();
        vdmon[i].print();
        cout << endl; 
    }
*/

    plot_m(vmon,vdmon,small);

}

void info_select(data d,int &year, int &mon, int  &day, int &time){
   
   string hour, date;
  
   hour =  d.Hora_id.substr(1,2);
    date =  d.Data_id;
   
    year = atoi(date.substr(1, 4).c_str());
    mon = atoi(date.substr(6, 7).c_str());
    day = atoi(date.substr(9, 10).c_str());
    time = atoi(hour.c_str());

    
}

void info_select(ddata d,int &year, int &mon, int  &day, int &time){
   
   string hour, date;
  
 //  hour =  d.Hora_id.substr(1,2);
    date =  d.Data_id;
   
    year = atoi(date.substr(1, 4).c_str());
    mon = atoi(date.substr(6, 7).c_str());
    day = atoi(date.substr(9, 10).c_str());
    time = atoi(hour.c_str());

    
}


void plot_m(vector<ddata> val, vector<ddata> err, bool small){
    
        
 /*   for(int i =0; i<val.size(); i++)
    {
        val[i].print();
        err[i].print();
        cout << endl; 
    }*/
    
    
  //  bool small = true ; // para setear los labels  
    
    int year,mon,day,time,min,sec;
    min =0;
    sec=0;
    
    info_select(val[0],year,mon,day, time); 
    time = 0;
    TGraphErrors *gr = new TGraphErrors(val.size()-1);
    
    TDatime *fecha = new TDatime() ; // constructor (Int_t year, Int_t month, Int_t day, Int_t hour, Int_t min, Int_t sec)
    
   // cout << fecha->GetDate() <<endl;
   
   
    vector<string > pol_name = {"Pm10", "Pm25", "NO", "NO2", "NOx", "SO2", "O3" };   
    
    vector<double> pol_val;
    vector<double> pol_err;
    
    pol_val.reserve(7);
    pol_err.reserve(7);
    
    vector<TGraphErrors> grs; 
    
    grs.reserve(7);
    
    TGraphErrors ngr =  TGraphErrors(val.size()-1);
    TGraphErrors ngr1 =  TGraphErrors(val.size()-1);
    TGraphErrors ngr2 =  TGraphErrors(val.size()-1);
    TGraphErrors ngr3 =  TGraphErrors(val.size()-1);
    TGraphErrors ngr4 =  TGraphErrors(val.size()-1);
    TGraphErrors ngr5 =  TGraphErrors(val.size()-1);
    TGraphErrors ngr6 =  TGraphErrors(val.size()-1);

    grs.push_back(ngr);
    grs.push_back(ngr1);
    grs.push_back(ngr2);
    grs.push_back(ngr3);
    grs.push_back(ngr4);
    grs.push_back(ngr5);
    grs.push_back(ngr6);
    
    
    for (int i = 0; i < 7; i++){
       grs[i].SetTitle(pol_name[i].c_str());
    }
    
    
    
    
  /*  for (int i = 0; i < pol.size(); i++){
        
        cout << pol[i] <<endl;
    }*/
   
    
  /*  val[0].print();
    val[0].get_data(pol_val);

        
    
    cout << "testting vector "<< endl;
    
    for (int i = 0; i < 7; i++){
        
        cout << pol_val[i] <<endl;
    } */
    
  
    
     
  
    for(int i =0; i<val.size(); i++){
    
        info_select(val[i],year,mon,day, time);     
        fecha->Set(year,mon,1,0,min,sec);
    
        val[i].get_data(pol_val);
        err[i].get_data(pol_err);
        
        
    
        for (int j = 0; j < 7; j++){
            
             
            
            
            if (pol_val[j]<10000 || pol_val[j] == 0){
                grs[j].SetPoint(i,fecha->Convert(),pol_val[j]);
                if (pol_err[j]<10000)
                    grs[j].SetPointError(i,0,pol_err[j]);
                else 
                    grs[j].SetPointError(i,0,0.1);    
            
        //    cout << i << " " << fecha->Convert() << " "  << pol_val[j] << j << endl;
           
            if(j==1){
          //  cout << "esta pinga ue es " << pol_val[1] << " " << pol_err[1] << endl;
              ngr.SetPoint(i,fecha->Convert(),pol_val[j]);  
         //     cout << "esta pinga que es  " << i <<  " " << fecha->Convert() << " " << pol_val[j] << " " << pol_err[j] <<  endl;   
                }
            
          
            }
            else continue;
                
            }
            
        }
    
        
     TCanvas *c1 = new TCanvas("c1","",800,600) ;
        
        ngr.Draw("AP");
        grs_form(ngr);
        c1->Print("test.jpg"); 
     delete c1;
     

     string name ;
    // gr_form(gr);
    // gr->SetTitle(name.c_str());
     
     for (int i = 0; i < 7 ; i++ ){
    
     TCanvas *c1 = new TCanvas("c1","",800,600) ;
    
     if(small)
        name =  val[0].Estacao_id2.substr(1,val[0].Estacao_id2.size()-2)+ " " + pol_name[i].c_str() + " 11-18h" ;
     else
        name =  val[0].Estacao_id2.substr(1,val[0].Estacao_id2.size()-2)+ " " + pol_name[i].c_str();
     grs[i].SetTitle(name.c_str());
     grs_form(grs[i]);
     grs[i].Draw("AP");

     
     name =  val[0].Estacao_id2.substr(1,val[0].Estacao_id2.size()-2) + pol_name[i].c_str() +".pdf";
     
     cout << name << endl;
     c1->Print(name.c_str());
     
 
    delete c1;    
    }
    
    delete fecha;
    delete gr;
    
    
    
    
}


void gr_form(TGraphErrors* gr){
    
     TDatime *fecha = new TDatime(2012,1,1,0,0,0);    
     TDatime *fecha1 = new TDatime(2016,1,1,0,0,0);
    
    
    gr->SetMarkerStyle(24);
    gr->GetXaxis()->SetTimeDisplay(1);
    gr->GetXaxis()->SetNdivisions(5,6,2);
    gr->GetXaxis()->SetRangeUser(fecha->Convert(),fecha1->Convert());
    gr->GetXaxis()->SetTimeFormat("%Y-%m");
    gr->GetXaxis()->SetTimeOffset(0,"gmt");
    
    delete fecha;
    delete fecha1;
  
    
}

void grs_form(TGraphErrors& gr){
    
     TDatime *fecha = new TDatime(2012,1,1,0,0,0);    
     TDatime *fecha1 = new TDatime(2016,1,1,0,0,0);
    
    
    gr.SetMarkerStyle(24);
    gr.GetXaxis()->SetTimeDisplay(1);
    gr.GetXaxis()->SetNdivisions(5,6,2);
    gr.GetXaxis()->SetRangeUser(fecha->Convert(),fecha1->Convert());
    gr.GetXaxis()->SetTimeFormat("%Y-%m");
    gr.GetXaxis()->SetTimeOffset(0,"gmt");
    
    delete fecha;
    delete fecha1;
  
    
}



void mon_hour_selector(std::vector<data>& v, int hora, std::vector<ddata>& vmon, std::vector<ddata>& vdmon ){
    
   int year, mon, day, time;
   int dmon;
      
   
   ddata mes;
   ddata counter;
   mes.setceros(); 
   counter.setceros();
   

   info_select(v[0],year,mon,day, time); 
   dmon = mon;
   mes.readbasics(v[0]);
   
   for(int i = 0; i < v.size() ; i++){
       
    info_select(v[i],year,mon,day, time); 

    if(year<2012)
        continue;
    
    if(time!=hora)
        continue;
    
    if(dmon==mon){
        mes.esum(v[i],counter);
    //    cout << "siii" <<endl;
    }
   else {
       mes.edivision(counter);
       vmon.push_back(mes);
    //   cout << "nooo " << endl;
       dmon = mon;
       counter.setceros();
       mes.setceros();
       mes.readbasics(v[i]);
       mes.esum(v[i],counter);
    }
    
   }
    
//_______________  SIGMA CALCULATIONS _____________________ 
    
     info_select(v[0],year,mon,day, time); 
     dmon = mon;
     mes.readbasics(v[0]);
     counter.setceros();

    mes.setceros();
  //  mes.print();
  //  cout << endl;
    for(int i = 0; i < v.size() ; i++){ // v.size()
       
    info_select(v[i],year,mon,day, time); 
    
    if(year<2012)
        continue;

    if(dmon==mon){
        mes.e2sum(v[i],vmon[mon-1],counter);
    }
   else {
       mes.edivision(counter);
       mes.esqrt();
       vdmon.push_back(mes);
       dmon = mon;
       counter.setceros();
       mes.setceros();
       mes.readbasics(v[i]);
       mes.setceros();
       mes.e2sum(v[i],vmon[mon-1],counter);
    }
   
}

//    plot_m(vmon,vdmon,small);

  
    
    
}
   
void mon_hour_plotter(vector<vector<ddata>> val, vector<vector<ddata>> error){
    cout << "mytest" << endl; 

    string name;
    
   // cout << val.size() ;
    
    int year,mon,day,time,min,sec;
    
    vector<string > pol_name = {"Pm10", "Pm25", "NO", "NO2", "NOx", "SO2", "O3" };   
    vector<string > mes = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };  
    
    
    vector<double> pol_val;
    vector<double> pol_err;

    pol_val.reserve(7);
    pol_err.reserve(7);
    
    min =0;
    sec=0;
    
    
    vector<TGraphErrors> grs; 
    
    for(int i = 0; i < 47; i++ ){ // me muevo por los meses 
        
        for(int l=0; l<7; l++)
            grs.push_back( TGraphErrors(24)); 
        
        for(int j=0; j < 24; j++) {  // me muevo por las horas 
                info_select(val[j][i],year,mon,day, time);        
                if(year < 2012)
                    continue;
                //  cout << year << "  " << mon << "  " << "i " << i << " j " << j+1 << endl; 
                
                val[j][i].get_data(pol_val);
                error[j][i].get_data(pol_err);
                
                for(int k=0; k<7; k++){
                    if(pol_val[k]<1000 && pol_val[k]>=0){
                        grs[k].SetPoint(j,j+1,pol_val[k]);
                        grs[k].SetPointError(j,0,pol_err[k]);
                        grs[k].SetMarkerStyle(24);
                        cout << k << " " << j+1 << " " << pol_val[k] << endl;
                        }
                   
                }
            
        }
        
        for(int l=0; l<7; l++)  {
            TCanvas *c = new TCanvas("c1","",800,600); 
            name=  pol_name[l] + " " +  mes[mon-1] + " " + to_string(year) ;
            grs[l].SetTitle(name.c_str());
            grs[l].Draw("APl");
            name = to_string(year)+ to_string(mon)+pol_name[l]+val[0][0].Estacao_id2.substr(1,val[0][0].Estacao_id2.size()-2)+".pdf";
            c->Print(name.c_str()); 
            
            delete c; 
            
        }
        
        
        TMultiGraph *mg = new TMultiGraph();
        
        TCanvas *c = new TCanvas("c1","",800,600); 
        mg->Add(&grs[1]);
        mg->Add(&grs[3]);
        mg->Add(&grs[6]);
        mg->Draw("ALP");
        
        name=   mes[mon-1] + " " + to_string(year) ;
        mg->SetTitle(name.c_str());
        name=to_string(year)+ to_string(mon)+val[0][0].Estacao_id2.substr(1,val[0][0].Estacao_id2.size()-2)+".pdf";
        grs[1].SetMarkerColor(4);
        grs[1].SetLineColor(4);
        grs[3].SetMarkerStyle(33);
        grs[3].SetMarkerColor(2);
        grs[3].SetLineColor(2);
        grs[6].SetMarkerStyle(8);
        grs[6].SetMarkerColor(3);
        grs[6].SetLineColor(3);
        mg->GetXaxis()->SetRangeUser(1,24);
        mg->GetXaxis()->SetNdivisions(24,1,1);
        mg->GetYaxis()->SetRangeUser(0,150);        
        
        TLegend *l = new TLegend(0.8,.75,.9,.9);
        l->AddEntry(&grs[1],pol_name[1].c_str(),"lp");
        l->AddEntry(&grs[3],pol_name[3].c_str(),"lp");
        l->AddEntry(&grs[6],pol_name[6].c_str(),"lp");
        l->Draw();
        
        c->Print(name.c_str());
        
        
        delete l;
        delete c; 
        delete mg;
        
        
        
        grs.erase(grs.begin(),grs.end());
        cout << "grs size " << grs.size() << endl;
        
    }
    
    
    
}   
    
  



