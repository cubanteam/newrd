#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <string>


//#include "DataHelper.hh"

#include "TROOT.h"
#include "TBrowser.h"
#include "Riostream.h"
#include "TH1.h"
#include "TFile.h"
#include "TRandom.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"
#include "TGraphErrors.h"
#include "TList.h"
#include "TObjArray.h"
#include "TChain.h"
#include "TSystem.h"
#include "TMath.h"
#include "TVector3.h"
#include "TCanvas.h"
#include "TRegexp.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TDatime.h"

struct data{
    string code;
    string Estacao_id;
    string Estacao_id2; 
    string Hora_id;
    string Data_id;
    string pm10medio; 
    string pm25medio;
    string NOmedio; 
    string NO2medio; 
    string NOxmedio; 
    string SO2medio; 
    string O3medio;
    void print();
    
};


struct ddata{
    string code;
    string Estacao_id;
    string Estacao_id2; 
    string Data_id;
    
    double pm10medio; 
    double pm25medio;
    double NOmedio; 
    double NO2medio; 
    double NOxmedio; 
    double SO2medio; 
    double O3medio;
 
    void print();
    void setceros();
    void readbasics(data);
    void esum (data, ddata& ); 
    void e2sum(data d, ddata mean, ddata& dd);
    void edivision(ddata);
    void esqrt();
    void get_data(vector<double> & v);
    
    
    ddata() 
       // : pm10medio(pm10medio), pm25medio(pm25medio), NOmedio(NOmedio), NO2medio(NO2medio), NOxmedio(NOxmedio), SO2medio(SO2medio), O3medio(O3medio)
    {
    }
    
    ddata(double pm10medio, double pm25medio, double NOmedio,double NO2medio,double NOxmedio,double SO2medio,double O3medio ) 
        : pm10medio(pm10medio), pm25medio(pm25medio), NOmedio(NOmedio), NO2medio(NO2medio), NOxmedio(NOxmedio), SO2medio(SO2medio), O3medio(O3medio)
    {
    }
    
    ddata operator-(const ddata& a) const
    {
        return ddata(pm10medio-a.pm10medio, pm25medio-a.pm25medio, NOmedio-a.NOmedio, NO2medio-a.NO2medio, NOxmedio-a.NOxmedio, SO2medio-a.SO2medio, O3medio-a.O3medio );
    }
    
    ddata operator-(const double& a) const
    {
        return ddata(pm10medio-a, pm25medio-a, NOmedio-a, NO2medio-a, NOxmedio-a, SO2medio-a, O3medio-a );
    }
    
};

void data::print()
{
    cout << code <<","<< Estacao_id <<","<< Estacao_id2 <<","<< Hora_id <<","<< Data_id <<","<< pm10medio <<","<< pm25medio <<","<< NOmedio <<","<< NO2medio <<","<< NOxmedio <<","<< SO2medio <<","<< O3medio <<endl;    
}


void ddata::setceros()
{
 
    pm10medio=0; 
    pm25medio=0;
    NOmedio=0; 
    NO2medio=0; 
    NOxmedio=0; 
    SO2medio=0; 
    O3medio=0;
}

void ddata::readbasics(data d)
{
    code=d.code;
    Estacao_id = d.Estacao_id;
    Estacao_id2 = d.Estacao_id2;
    Data_id = d.Data_id;
    
}

 void ddata::esum(data d, ddata& dd)
{
    
 string helper;
    
  if(d.pm10medio!="NA"){
   //   cout << "tem dados " << endl; 
      helper = d.pm10medio;
      pm10medio = pm10medio + atof(helper.c_str());
      dd.pm10medio += 1.0;
  //    cout << "new pm10medio " << pm10medio <<endl;
  }
    
  if(d.pm25medio!="NA"){
      helper = d.pm25medio;
      pm25medio = pm25medio + atof(helper.c_str());
      dd.pm25medio += 1.0;
  //    cout << "tem dados " << endl; 
  }
  
    
  if(d.NOmedio!="NA"){
      helper = d.NOmedio;
      NOmedio = NOmedio + atof(helper.c_str());
      dd.NOmedio += 1.0;
     // cout << "tem dados " << endl; 
  }
 


if(d.NO2medio!="NA"){
      helper = d.NO2medio;
      NO2medio = NO2medio + atof(helper.c_str());   
      dd.NO2medio += 1.0;

//      cout << "tem dados " << endl; 
  }
  
  
if(d.NOxmedio!="NA"){
      helper = d.NOxmedio;
      NOxmedio = NOxmedio + atof(helper.c_str());  
      dd.NOxmedio += 1.0;

      //cout << "tem dados " << endl; 
  }
     

if(d.SO2medio!="NA"){
      helper = d.SO2medio;
      SO2medio = SO2medio + atof(helper.c_str());  
      dd.SO2medio += 1.0;
  }


if(d.O3medio!="NA"){
      helper = d.O3medio;
      O3medio = O3medio + atof(helper.c_str());  
      dd.O3medio =  dd.O3medio + 1.0;
  }
  
     
}
     
     
     
void ddata::e2sum(data d, ddata mean, ddata& dd)
{
    
 string helper;
 double tval;
    
  if(d.pm10medio!="NA"){
   //   cout << "tem dados " << endl; 
      helper = d.pm10medio;
      tval = atof(helper.c_str());
      pm10medio = pm10medio + (tval-mean.pm10medio)*(tval-mean.pm10medio);
      dd.pm10medio += 1.0;
  //    cout << "new pm10medio " << pm10medio <<endl;
  }
    
  if(d.pm25medio!="NA"){
      helper = d.pm25medio;
      tval = atof(helper.c_str());
      pm25medio = pm25medio + (tval-mean.pm25medio)*(tval-mean.pm25medio);
      dd.pm25medio += 1.0;
  //    cout << "tem dados " << endl; 
  }
  
    
  if(d.NOmedio!="NA"){
      helper = d.NOmedio;
      tval = atof(helper.c_str());
      NOmedio = NOmedio + (tval-mean.NOmedio)*(tval-mean.NOmedio);
      NOmedio = NOmedio + atof(helper.c_str());
      dd.NOmedio += 1.0;
     // cout << "tem dados " << endl; 
  }
 


if(d.NO2medio!="NA"){
      helper = d.NO2medio;
      tval = atof(helper.c_str());
      NO2medio = NO2medio + (tval-mean.NO2medio)*(tval-mean.NO2medio); 
      dd.NO2medio += 1.0;

//      cout << "tem dados " << endl; 
  }
  
  
if(d.NOxmedio!="NA"){
      helper = d.NOxmedio;
      tval = atof(helper.c_str());
      NOxmedio = NOxmedio + (tval-mean.NOxmedio)*(tval-mean.NOxmedio);
      dd.NOxmedio += 1.0;

      //cout << "tem dados " << endl; 
  }
     

if(d.SO2medio!="NA"){
      helper = d.SO2medio;
      tval = atof(helper.c_str());
      SO2medio = SO2medio + (tval-mean.SO2medio)*(tval-mean.SO2medio);
      dd.SO2medio += 1.0;
  }


if(d.O3medio!="NA"){
      helper = d.O3medio;
      tval = atof(helper.c_str());
      O3medio = O3medio + (tval-mean.O3medio)*(tval-mean.O3medio);
      dd.O3medio =  dd.O3medio + 1.0;
  }
  
     
}     

void ddata::print()
{
    cout << code <<","<< Estacao_id <<","<< Estacao_id2 <<","<< Data_id <<","<< pm10medio <<","<< pm25medio <<","<< NOmedio <<","<< NO2medio <<","<< NOxmedio <<","<< SO2medio <<","<< O3medio <<endl;    
}


void ddata::edivision(ddata dd)
{
    if (pm10medio==0)
    pm10medio=0;
    else
            pm10medio=pm10medio/dd.pm10medio;
    
    if (pm25medio==0)
    pm25medio=0;
    else
            pm25medio=pm25medio/dd.pm25medio;
    
    if(NOmedio==0)
        NOmedio=0;
    else
        NOmedio=NOmedio/dd.NOmedio; 
    NO2medio=NO2medio/dd.NO2medio; 
    NOxmedio=NOxmedio/dd.NOxmedio; 
    SO2medio=SO2medio/dd.SO2medio; 
    O3medio=O3medio/dd.O3medio;
}

void ddata::esqrt()
{
    pm10medio=sqrt(pm10medio); 
    pm25medio=sqrt(pm25medio); 
    NOmedio=sqrt(NOmedio); 
    NO2medio=sqrt(NO2medio); 
    NOxmedio=sqrt(NOxmedio); 
    SO2medio=sqrt(SO2medio); 
    O3medio=sqrt(O3medio);
}



void ddata::get_data(vector<double>& v)
{
  // 
    v[0] = pm10medio;
    v[1] = pm25medio;
    v[2] = NOmedio;
    v[3] = NO2medio;
    v[4] = NOxmedio;
    v[5] = SO2medio;
    v[6] = O3medio;
    
  //  cout << "Set values" << endl;
    
}



void read_data(std::vector<data>& v,std::vector<data>& v1);

void daily_selector(std::vector<data>& v);

void info_select(data d,int &year, int &mon, int  &day, int &time);

void info_select(ddata d,int &year, int &mon, int  &day, int &time);

void mon_selector(std::vector<data>& v, bool small);


void plot_m(vector<ddata> val, vector<ddata> err, bool small);

void gr_form(TGraphErrors* gr);

void grs_form(TGraphErrors& gr);

void plot_d(vector<ddata> val, vector<ddata> err);

void mon_hour_selector(std::vector<data>& v, int hora, std::vector<ddata>& vmon, std::vector<ddata>& vdmon );
void mon_hour_plotter(vector<vector<ddata>> val, vector<vector<ddata>> error);



